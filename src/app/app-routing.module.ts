import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './modules/features/login/login.component';
import { authGuard } from './core/guards/auth.guard';
import { NotFoundComponent } from './modules/features/not-found/not-found.component';

const routes: Routes = [
  { path: '', component: LoginComponent },
  { path: 'login', component: LoginComponent },
  { 
    path: 'employee',
    canActivate: [authGuard],
    loadChildren: () => import('./modules/features/employee/employee.modules').then(mod => mod.EmployeeModule)
  },
  { path: '404', component: NotFoundComponent },
  { path: '**', redirectTo: '/404' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
