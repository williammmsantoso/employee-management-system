export interface GroupModel {
    value: string,
    display: string,
}