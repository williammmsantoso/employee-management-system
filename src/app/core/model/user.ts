export interface EmployeeModel {
    birthDate: Date,
    description: string,
    email: string,
    firstName: string,
    gender: string,
    group: string,
    id: string,
    lastName: string,
    password: string,
    salary: string | number
    status: string,
}