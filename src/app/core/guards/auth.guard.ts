import { CanActivateFn, Router } from '@angular/router';
import { AuthService } from '../../shared/services/auth.service';
import { ToastrService } from 'ngx-toastr';
import { inject } from '@angular/core';

export const authGuard: CanActivateFn = (route, state) => {
  const router = inject(Router);
  const service = inject(AuthService);
  const toastr = inject(ToastrService);

  if (service.isLoggedIn()) {
    return true;
  } else {
    toastr.warning('You need to login first, to access this page!');
    router.navigate(['']);
    return false;
  }  
};
