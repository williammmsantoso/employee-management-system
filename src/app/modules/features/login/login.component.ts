import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from '../../../shared/services/auth.service';
import { Router } from '@angular/router';
import { EncrDecrServiceService } from '../../../shared/services/encr-decr-service.service';
import { encrDecrKey } from '../../../core/constants/encrDecr';
import { catchError } from 'rxjs';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {
  constructor(
    private builder: FormBuilder,
    private toastr: ToastrService,
    private service: AuthService,
    private router: Router,
    private encrDecr: EncrDecrServiceService,
  ) {
    sessionStorage.clear();
  }

  userData: any;

  loginForm = this.builder.group({
    username: this.builder.control('', Validators.required),
    password: this.builder.control('', Validators.required)
  })

  proceedLogin() {
    if (this.loginForm.valid) {
      this.service.getByCode(this.loginForm.value.username)
        .pipe(catchError((err) => {
          this.toastr.error('Username or Password is wrong!');
          return err;
        }))
        .subscribe((res) => {
          this.userData = res;
          let decrypted = this.encrDecr.decrypt(encrDecrKey, this.userData.password);
          if (decrypted === this.loginForm.value.password) {
            delete this.userData.password;
            sessionStorage.setItem('user', this.userData);
            this.toastr.success(`Welcome, ${this.userData.firstName} ${this.userData.lastName}`);
            this.router.navigate(['/employee']);
          } else {
            this.toastr.error('Username or Password is wrong!');
          }
        })

    } else {
      this.toastr.error('Username or Password is wrong!');
    }
  }
}
