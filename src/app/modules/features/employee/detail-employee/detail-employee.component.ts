import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../../shared/services/auth.service';
import { ActivatedRoute } from '@angular/router';
import { EmployeeModel } from '../../../../core/model/user';

@Component({
  selector: 'app-detail-employee',
  templateUrl: './detail-employee.component.html',
  styleUrls: ['./detail-employee.component.scss']
})
export class DetailEmployeeComponent implements OnInit {
  constructor(
    private route: ActivatedRoute,
    private service: AuthService,
  ) {}

  data!: EmployeeModel;
  routeSub: any;
  avatar: any;

  ngOnInit(): void {
    this.loadEmployee();
  }

  ngOnDestroy() {
    this.routeSub.unsubscribe();
  }

  async loadEmployee() {
    let id;
    this.routeSub = this.route.params.subscribe((params) => {
      id = params['id'];
    });

    await this.service.getByCode(id).subscribe((res) => {
      this.data = res as EmployeeModel;
      this.avatar = this.data.gender === "Male" ? "/assets/male-avatar.svg" : "/assets/female-avatar.svg";
    });
  }
}
