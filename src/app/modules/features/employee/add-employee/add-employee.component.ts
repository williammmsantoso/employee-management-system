import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormControl, ValidationErrors, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from '../../../../shared/services/auth.service';
import { Router } from '@angular/router';
import { Observable, filter, map, startWith } from 'rxjs';
import { MasterService } from '../../../../shared/services/master.service';
import { EncrDecrServiceService } from '../../../../shared/services/encr-decr-service.service';
import { encrDecrKey } from '../../../../core/constants/encrDecr';
import { GroupModel } from '../../../../core/model/master';
import { passwordPattern } from 'src/app/core/constants/validator';

@Component({
  selector: 'app-add-employee',
  templateUrl: './add-employee.component.html',
  styleUrls: ['./add-employee.component.scss']
})
export class AddEmployeeComponent implements OnInit {
    constructor(
      private builder: FormBuilder,
      private toastr: ToastrService,
      private service: AuthService,
      private masterService: MasterService,
      private router: Router,
      private encrDecr: EncrDecrServiceService,
    ) {}
    
    groupList!: GroupModel[];
    filterOptions !: Observable<GroupModel[]>
    maxDate = new Date();

    errorsPassword = [
      {
        key: "required",
        message: "Password is required."
      },
      {
        key: "passwordValidator",
        message: "Password need to include lowercase, uppercase, at lease one number or symbol and minimal 8 Character."
      }
    ];
    errorsEmail = [
      {
        key: "required",
        message: "Email is required."
      },
      {
        key: "email",
        message: "Email is not valid."
      }
    ];

    ngOnInit(): void {
      this.loadGroup();
    }

    addEmployeeForm = this.builder.group({
      id: this.builder.control('', Validators.required),
      firstName: this.builder.control('', Validators.required),
      lastName: this.builder.control('', Validators.required),
      gender: this.builder.control('Male', Validators.required),
      description: this.builder.control('', Validators.required),
      birthDate: this.builder.control('', Validators.required),
      email: this.builder.control('', Validators.compose([ Validators.required, Validators.email ])),
      password: this.builder.control('', Validators.compose([ Validators.required, this.passwordValidator() ])),
      salary: this.builder.control('', Validators.required),
      status: this.builder.control('Permanent', Validators.required),
      group: this.builder.control('', Validators.required),
    });

    proceedAddEmployee() {
      if (this.addEmployeeForm.valid) {
        this.service.registerEmployee({
          ...this.addEmployeeForm.value,
          password: this.encrDecr.encrypt(encrDecrKey, this.addEmployeeForm.value.password)
        }).subscribe(res => {
          this.toastr.success('Registered Successfully');
          this.router.navigate(['/employee']);
        });
      } else {
        this.toastr.warning('Please enter valid data');
      }
    }

    loadGroup() {
      this.masterService.getAllGroup()
        .pipe(
          filter((item) => item !== null)
        )
        .subscribe(options => {
          this.groupList = options;
        })
    }

    filterGroup() {
      const _control = this.addEmployeeForm.get("group") as FormControl;
      this.filterOptions = _control.valueChanges.pipe(
        startWith(''), map(value =>  this._filter(_control.value || ''))
      )
    }

    errorGroup(type: "email" | "password", key: string) {
      if (type === "password") {
        const obj = this.errorsPassword.find((item) => item.key === key);

        return obj?.message;
      } else {
        const obj = this.errorsEmail.find((item) => item.key === key);

        return obj?.message;
      }
    }

    passwordValidator() {
      return (control: AbstractControl): ValidationErrors | null => {
        const password = control.value as string;
        if (password && !passwordPattern.test(password)) {
          return { 'passwordValidator': true };
        }
        return null;
      };
    }

    private _filter(value: string): GroupModel[] {
      const filterValue = value.toLowerCase();

      return this.groupList.filter(option => option.display.toLowerCase().includes(filterValue));
    }

    testClickFunction() {
      alert('Add Employee Clicked!');
    }
}
