import { Component, ViewChild } from '@angular/core';
import { AuthService } from '../../../../shared/services/auth.service';
import { MatDialog } from '@angular/material/dialog';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { UpdatePopupComponent } from '../../../../shared/components/update-popup/update-popup.component';
import { ConfirmationDeleteComponent } from '../../../../shared/components/confirmation-delete/confirmation-delete.component';

@Component({
  selector: 'app-list-employee',
  templateUrl: './list-employee.component.html',
  styleUrls: ['./list-employee.component.scss']
})
export class ListEmployeeComponent {
  constructor(
    private service: AuthService,
    private dialog: MatDialog,
    private builder: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private router: Router,
  ) {
    this.activatedRoute.queryParams.subscribe((data: any) => {
      if (
        (data.username !== null && data.username !== "" && data.username !== undefined) ||
        (data.firstName !== null && data.firstName !== "" && data.firstName !== undefined) ||
        (data.lastName !== null && data.lastName !== "" && data.lastName !== undefined)
      ) {
        this.searchValueUsername = data.username;
        this.searchValueFirstName = data.firstName;
        this.searchValueLastName = data.lastName;

        this.searchEmployee();
      } else {
        this.loadEmployee();
      }
    })
  }

  employeeList: any;
  dataSource: any;
  @ViewChild(MatPaginator) paginator !:MatPaginator;
  @ViewChild(MatSort) sort !:MatSort;
  searchEmployeeForm!: FormGroup;
  searchValueUsername = '';
  searchValueFirstName = '';
  searchValueLastName = '';

  displayedColumns: string[] = ['username', 'firstName', 'lastName', 'email', 'gender', 'status', 'birthDate', 'group', 'action'];

  ngOnInit(): void {
    this.searchEmployeeForm = this.builder.group({
      username: this.builder.control(this.searchValueUsername),
      firstName: this.builder.control(this.searchValueFirstName),
      lastName: this.builder.control(this.searchValueLastName),
    });
  }

  async loadEmployee() {
    await this.service.getAll().subscribe(res => {
      this.employeeList = res;
      this.dataSource = new MatTableDataSource(this.employeeList);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }

  async searchEmployee() {
    const username = this.searchEmployeeForm?.get('username')?.value || this.searchValueUsername;
    const firstName = this.searchEmployeeForm?.get('firstName')?.value || this.searchValueFirstName;
    const lastName = this.searchEmployeeForm?.get('lastName')?.value || this.searchValueLastName;


    this.router.navigate(
      [],
      {
        relativeTo: this.activatedRoute,
        queryParams: {
          username: username,
          firstName: firstName,
          lastName: lastName,
        },
        queryParamsHandling: 'merge',
      }
    );
    
    await this.service.getEmployeeByParams(username, firstName, lastName).subscribe((res) => {
      this.employeeList = res;
      this.dataSource = new MatTableDataSource(this.employeeList);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    })
  }

  updateEmployee(code: any) {
    const popup = this.dialog.open(UpdatePopupComponent, {
      enterAnimationDuration: '1000ms',
      exitAnimationDuration: '500ms',
      width: '65%',
      height: '90%',
      data: {
        userCode: code,
      }
    });
    popup.afterClosed().subscribe((res) => {
      this.loadEmployee();
    });
  }

  deleteEmployee(code: any) {
    const popup = this.dialog.open(ConfirmationDeleteComponent, {
      enterAnimationDuration: '1000ms',
      exitAnimationDuration: '500ms',
      width: '30%',
      data: {
        userCode: code,
      }
    });

    popup.afterClosed().subscribe((res) => {
      this.loadEmployee();
    })
  }

  resetInput() {
    this.searchEmployeeForm.get('username')?.setValue('');
    this.searchEmployeeForm.get('firstName')?.setValue('');
    this.searchEmployeeForm.get('lastName')?.setValue('');

    this.router.navigate(
      [],
      {
        relativeTo: this.activatedRoute,
        queryParams: {
          username: '',
          firstName: '',
          lastName: '',
        },
        queryParamsHandling: 'merge',
      }
    );

    this.loadEmployee();
  }

  testClickFunction() {
    alert('List Employee Clicked!');
  }
}
