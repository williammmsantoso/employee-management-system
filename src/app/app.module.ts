import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from 'src/material.module';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { ToastrModule } from 'ngx-toastr';
import { LoginComponent } from './modules/features/login/login.component';
import { UpdatePopupComponent } from './shared/components/update-popup/update-popup.component';
import { AddEmployeeComponent } from './modules/features/employee/add-employee/add-employee.component';
import { EncrDecrServiceService } from './shared/services/encr-decr-service.service';
import { DetailEmployeeComponent } from './modules/features/employee/detail-employee/detail-employee.component';
import { NotFoundComponent } from './modules/features/not-found/not-found.component';
import { ConfirmationDeleteComponent } from './shared/components/confirmation-delete/confirmation-delete.component';
import { ListEmployeeComponent } from './modules/features/employee/list-employee/list-employee.component';
import { TitleSectionComponent } from './shared/components/title-section/title-section.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    UpdatePopupComponent,
    AddEmployeeComponent,
    ConfirmationDeleteComponent,
    DetailEmployeeComponent,
    NotFoundComponent,
    ListEmployeeComponent,
    TitleSectionComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    ReactiveFormsModule,
    HttpClientModule,
    ToastrModule.forRoot(),
  ],
  providers: [
    EncrDecrServiceService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
