import { Component, DoCheck } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements DoCheck {
  title = 'employee-management-system';
  isMenuRequired = false;

  constructor(private router: Router) {}

  ngDoCheck(): void {
    let currentUrl = this.router.url;
    let firstUrl = currentUrl.split('/');
    let getCurrentUrl = firstUrl[1].split('?');

    if (getCurrentUrl[0] === 'employee') {
      this.isMenuRequired = true;
    } else {
      this.isMenuRequired = false;
    }
  }
}
