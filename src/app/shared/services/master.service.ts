import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { GroupModel } from '../../core/model/master';

@Injectable({
  providedIn: 'root'
})
export class MasterService {

  constructor(private http: HttpClient) { }
  baseUrl = 'http://localhost:3000';

  getAllGroup(): Observable<GroupModel[]> {
    return this.http.get<GroupModel[]>(this.baseUrl+'/groups');
  }
}
