import { Injectable } from '@angular/core';
import * as CryptoJS from 'crypto-js';

@Injectable({
  providedIn: 'root'
})
export class EncrDecrServiceService {

  constructor() { }

  encrypt(keys: string, value: any) {
    return CryptoJS.AES.encrypt(value.trim(), keys.trim()).toString();
  }

  decrypt(keys: string, value: any) {
    return CryptoJS.AES.decrypt(value.trim(), keys.trim()).toString(CryptoJS.enc.Utf8);
  }

}
