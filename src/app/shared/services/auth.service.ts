import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient) { }
  apiUrl = 'http://localhost:3000/employee';

  getAll(){
    return this.http.get(this.apiUrl)
  }

  getByCode(code: any) {
    return this.http.get(this.apiUrl+'/'+code);
  }

  getEmployeeByParams(username: any, firstName: string, lastName: string) {
    return this.http.get(this.apiUrl+'?id_like='+username + '&firstName_like='+firstName + '&lastName_like'+lastName);
  }

  registerEmployee(inputdata: any) {
    return this.http.post(this.apiUrl, inputdata);
  }

  updateEmployee(code: any, inputdata: any) {
    return this.http.put(this.apiUrl+'/'+code, inputdata);
  }

  deleteEmployee(code: any) {
    return this.http.delete(this.apiUrl+'/'+code);
  }

  isLoggedIn() {
    return sessionStorage.getItem('user') != null;
  }
}
