import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { Observable, filter, map, startWith } from 'rxjs';
import { MasterService } from '../../services/master.service';
import { AuthService } from '../../services/auth.service';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import { GroupModel } from '../../../core/model/master';
import { passwordPattern } from 'src/app/core/constants/validator';

@Component({
  selector: 'app-update-popup',
  templateUrl: './update-popup.component.html',
  styleUrls: ['./update-popup.component.scss']
})
export class UpdatePopupComponent implements OnInit {
  constructor(
    private builder: FormBuilder,
    private service: AuthService,
    private masterService: MasterService,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private toastr: ToastrService,
    private dialog: MatDialogRef<UpdatePopupComponent>
  ) {}

  errorsEmail = [
      {
        key: "required",
        message: "Email is required."
      },
      {
        key: "email",
        message: "Email is not valid."
      }
    ];

  async ngOnInit() {
    this.loadGroup();

    if (this.data.userCode !== null && this.data.userCode !== '') {
      await this.service.getByCode(this.data.userCode).subscribe((res) => {
        this.editData = res;
        this.editEmployeeForm.setValue({
          ...this.editData
        })
      })
    }
  }

  groupList!: GroupModel[];
  filterOptions !: Observable<GroupModel[]>
  editData: any;
  maxDate = new Date();

  editEmployeeForm = this.builder.group({
    id: this.builder.control('', Validators.required),
    firstName: this.builder.control('', Validators.required),
    lastName: this.builder.control('', Validators.required),
    gender: this.builder.control('Male', Validators.required),
    description: this.builder.control('', Validators.required),
    birthDate: this.builder.control('', Validators.required),
    email: this.builder.control('', Validators.compose([ Validators.required, Validators.email ])),
    password: this.builder.control('', Validators.required),
    salary: this.builder.control('', Validators.required),
    status: this.builder.control('Permanent', Validators.required),
    group: this.builder.control('', Validators.required),
  });

  loadGroup() {
    this.masterService.getAllGroup()
      .pipe(
        filter((item) => item !== null)
      )
      .subscribe(options => {
        this.groupList = options;
      })
  }

  filterGroup() {
    const _control = this.editEmployeeForm.get("group") as FormControl;
    this.filterOptions = _control.valueChanges.pipe(
      startWith(''), map(value =>  this._filter(_control.value || ''))
    )
  }

  private _filter(value: string): GroupModel[] {
    const filterValue = value.toLowerCase();

    return this.groupList.filter(option => option.display.toLowerCase().includes(filterValue));
  }

  errorGroup(type: "email" | "password", key: string) {
    const obj = this.errorsEmail.find((item) => item.key === key);

    return obj?.message;
  }
  
  proceedUpdateEmployee() {
    if (this.editEmployeeForm.valid) {
      this.service.updateEmployee(this.editEmployeeForm.value.id, this.editEmployeeForm.value).subscribe((res) => {
        this.toastr.success("Update Successfully!");
        this.closeDialog();
      })
    } else {
      this.toastr.warning('Please enter valid data');
    }
  }

  closeDialog() {
    this.dialog.close();
  }
}
