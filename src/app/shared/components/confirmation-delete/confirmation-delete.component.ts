import { Component, Inject } from '@angular/core';
import { AuthService } from '../../../shared/services/auth.service';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-confirmation-delete',
  templateUrl: './confirmation-delete.component.html',
  styleUrls: ['./confirmation-delete.component.scss']
})
export class ConfirmationDeleteComponent {
  constructor(
    private service: AuthService,
    private dialog: MatDialogRef<ConfirmationDeleteComponent>,
    private toastr: ToastrService,
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) {}

  closeDialog() {
    this.dialog.close();
  }

  proceedDeleteEmployee() {
    this.service.deleteEmployee(this.data.userCode).subscribe((res) => {
      this.toastr.success("Delete data successfully!");
      this.closeDialog();
    });
  }
}
